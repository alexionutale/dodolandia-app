const createError = require('http-errors')
const express = require('express')
const logger = require('morgan')
const admin = require('firebase-admin')// var will set admin to the global state
const cors = require('cors')
const wLogger = require('./utils/logger')
const indexRouter = require('./routes/index')
const apiRouter = require('./routes/api/layouts')
const apiUncompleted = require('./routes/api/uncompleted')
const errorHandler = require('./routes/error')

const app = express();

admin.initializeApp({
  credential: admin.credential.applicationDefault()
});

app.locals.admin = admin
global.wLogger = wLogger
app.use(cors())

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }))


app.use('/', indexRouter)
app.use('/api', apiRouter)
app.use('/api', apiUncompleted)

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404))
});

// error handler
app.use(errorHandler)

module.exports = app
