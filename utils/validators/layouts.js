const Joi = require('joi');
const createError = require('http-errors');
const Ajv = require('ajv');
const { required } = require('joi');


exports.scroll = (req, res, next) => {

  const schema = Joi.object({
    skip: Joi.number().min(0).max(99999).default(0),
    size: Joi.number().min(1).max(100).default(0),
    orderBy: Joi.string().valid('createdOn', 'updateOn','downloads'),
    asc: Joi.string().valid('-1', '1'),
    th: Joi.string().valid('TH13', 'TH12', 'TH11', 'TH10', 'TH9', 'TH8', 'TH7', 'TH6', 'TH5', 'TH4', 'TH3',),
    type: Joi.string().valid('HV', 'WB', 'BB'),
  })

  const valRes = schema.validate(req.query);
  console.log('valRes', valRes)
  if (valRes.error) next(createError(400, valRes.error))

  next()
}

// create a new layout validation 
exports.createLayout = (req, res, next) => {
  const schema = Joi.object({
    action: Joi.string().valid('OpenLayout'),
    description: Joi.string(),
    shortName: Joi.string(),
    longName: Joi.string(),
    link: Joi.string().pattern(new RegExp("^https:\\/\\/link\\.clashofclans\\.com.+action=OpenLayout")).required(),
    publicLink: Joi.string().pattern(new RegExp("^https:\\/\\/link\\.clashofclans\\.com.+action=OpenLayout")),
    share: Joi.string().valid('public', 'private'),
    tag: Joi.string(),
    tags: Joi.string(),
    th: Joi.string().pattern(new RegExp("^TH[0-9]{1,2}$")).required(),
    type: Joi.string().pattern(new RegExp("^(HV|WB|BB)$")).required(),
  })
}

exports.createLayout_ = (req, res, next) => {
  var ajv = new Ajv({ schemaId: 'id' }); // options can be passed, e.g. {allErrors: true}
  var schema = require('../schemas/create-layout.schema.json')
  ajv.addMetaSchema(require('ajv/lib/refs/json-schema-draft-04.json'));
  var validate = ajv.compile(schema);
  var valid = validate(req.body);
  if (!valid) console.log(validate.errors);

}

