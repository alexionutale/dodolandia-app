const express = require('express');
const createError = require('http-errors');
const router = express.Router();
const multer = require('multer')
const layoutsDB = require('../../mongodb/layouts.queries')
const favoritesDB = require('../../mongodb/favorites.queries')
const layoutsScrollDB = require('../../mongodb/layouts-scroll.queries')
const layoutsStorage = require('../../cloud-storage/layouts')
const validation = require('../../utils/validators/layouts')
const { logger: log } = require('../../utils/logger')
const { verifyToken, addUserAndAuthData } = require('./auth-midleware')

// create a new layout
const MULTER_UPLOAD_VALIDATION = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 20 * 1024 * 1024, // no larger than 20mb,
    files: 3,
    fields: 100
  },
})

/************************
 * ********** BEGIN API
 * *********************
 */

/**
 * Create A New Layout
 */
router.post('/layouts/new',
  verifyToken,
  MULTER_UPLOAD_VALIDATION.any(),
  async (req, res, next) => {
    try {
      // at least 1 image is required
      if (!req.files.length) throw createError(422, 'No image to upload.')

      const _id = layoutsDB.generateObjectId()
      const layout = req.body
      for (let i = 0; i < req.files.length; i++) {
        const image = req.files[i];
        console.log('received file:', image.originalname)
        await layoutsStorage.uploadImageFromData({ ...image, ...layout, uid: req.auth.uid, id: _id });
      }

      const insertRes = await layoutsDB.insert({ ...layout, id: _id }, req.auth)
      res.set({ ...insertRes, 'Access-Control-Expose-Headers': 'x-doc-id, x-layout-id' })
      res.status(201).send('layout with image saved\n')
    } catch (error) {
      // if (err instanceof multer.MulterError) {
      log.info(error)
      console.log(error)
      next(error)
    }
  })

/**
 * update a layout by doc._id
 */
router.put('/layouts/:id',
  verifyToken,
  MULTER_UPLOAD_VALIDATION.any(),
  async (req, res, next) => {
    try {
      if (req.params.id === undefined || req.params.id === null) throw createError(400, 'Please insert a valid params id.')
      if (!req.files.length) throw createError(422, 'No image to upload.')

      const layout = req.body
      const _id = req.params.id

      for (let i = 0; i < req.files.length; i++) {
        const image = req.files[i];
        console.log('received file:', image.originalname)
        await layoutsStorage.uploadImageFromData({ ...image, ...layout, uid: req.auth.uid, id: _id });
      }

      // TODO: is should be an update document in database instead of insert
      const insertRes = await layoutsDB.update({ ...layout, id: _id }, req.auth)
      res.set({ ...insertRes, 'Access-Control-Expose-Headers': 'x-doc-id, x-layout-id' })
      res.status(201).send('layout with image saved\n')
    } catch (error) {
      // if (err instanceof multer.MulterError)
      log.info(error)
      console.log(error)
      next(error);
    }
  })

/**
 * increase download counter for layout id
 */
router.put('/layouts/:id/downloads', async (req, res, next) => {
  try {
    const layoutId = req.params.id
    const incRes = await layoutsDB.increaseDownloads(layoutId)
    console.log(`layout ${layoutId} increase response:`, incRes)
    res.status(203).send('download increased\n');
  } catch (error) {
    log.info(error)
    console.log(error)
    next(error)
  }
})
/**
 * add new image to the layout
 */
// TODO: not in use, consider deleting this method
router.post('/layouts/:id/images',
  verifyToken,
  MULTER_UPLOAD_VALIDATION.any(),
  async (req, res, next) => {
    try {
      // if the array of files is 0, raise an error
      if (!req.files.length) throw createError(422, 'No image to upload.')

      const layout = await layoutsDB.findOne(req.params.id)
      // maybe check for the userID as well and just allow the owner of the layout to modify it
      if (!layout) throw createError(404, 'Layout not found')


      if (layout.uid !== req.auth.uid) throw createError(403, 'You must own the layout in order to upload an image')


      console.log(req.files)
      for (let i = 0; i < req.files.length; i++) {
        const image = req.files[i];
        await layoutsStorage.uploadImageFromData({ ...image, ...layout, uid: req.auth.uid });
      }

      res.status(201).send('file uploaded\n');
    } catch (error) {
      log.info(error)
      console.log(error)
      next(error)
    }
  });


/**
 * get normal layouts - 
 */ 
router.get('/layouts',
  addUserAndAuthData,
  validation.scroll,
  async (req, res, next) => {
    try {
      let { skip, size, orderBy, asc, th, type } = req.query
      let scrollResponse = []
      // if (req.auth) {
      scrollResponse = await layoutsScrollDB.scrollByIdWithFavorites(+skip, +size, orderBy, +asc, req.auth?.uid, th, type)

      res
        .set({
          'x-data-size': scrollResponse.length
        })
        .status(200)
        .json(scrollResponse);
    } catch (error) {
      next(error)
    }
  })

/**
 * get uncompleted layouts
 */
router.get('/layouts/uncompleted', validation.scroll, async (req, res, next) => {
  try {
    let { skip, size, orderBy, asc, th, type, uid } = req.query

    // (skip = 0, size = 10, orderBy = 'downloads', asc = -1, uid, th = 'TH13', type = undefined) 
    const scrollResponse = await layoutsScrollDB.scrollByIdUncompletedLayout(+skip, +size, orderBy, asc, uid, th, type)

    res
      .set({
        'x-data-size': scrollResponse.length
      })
      .status(200)
      .json(scrollResponse);

  } catch (error) {
    next(error)
  }
})

router.get('/layouts/:id', async (req, res, next) => {
  try {
    let { id } = req.params
    console.log(req.params)
    const scrollResponse = await layoutsScrollDB.getLayoutById(id)
    console.log("scrollResponse:", scrollResponse)
    if (!scrollResponse) {
      next(createError(404))
      return;
    }
    res
      .status(200)
      .json(scrollResponse);

  } catch (error) {
    next(error)
  }
})

// TODO: change end point to /users/me/favorites/toggle/layout/:id
router.put('/users/me/favorites/add/layout/:id',
  verifyToken,
  async (req, res, next) => {
    try {
      let { id: layoutId } = req.params
      let body = req.body
      console.log(req.params)
      const scrollResponse = await favoritesDB.toggleLayoutAsFavorite(body.isFavorite, layoutId, req.auth.uid)
      console.log("scrollResponse:", scrollResponse)

      res.status(200).json({ 'msg': 'layout added to favorite' });
    } catch (error) {
      next(error)
    }
  })

// get favorites as paginate results
router.get('/users/:uid/favorites',
  addUserAndAuthData,
  validation.scroll,
  async (req, res, next) => {
    try {
      let { skip, size, orderBy, asc, th, type } = req.query

      const scrollResponse = await favoritesDB.getAllFavoritesForUID(+skip, +size, orderBy, +asc, req.params.uid, th, type)
      console.log("scrollResponse:", scrollResponse)

      res
        .set({
          'x-data-size': scrollResponse.length
        })
        .status(200)
        .json(scrollResponse);
    } catch (error) {
      next(error)
    }
  })

/// group count th for favorites layouts
// the params for the moment is nor working on client side
// because of the way firebase works
// for the moment use the auth.uid from the token
router.get(
  '/users/:uid/favorites/categories',
  addUserAndAuthData,
  async (req, res, next) => {
    try {

      const groupFavoritesCountResponse = await favoritesDB.getGroupCountFavoritesTh(req.auth.uid)
      console.log("groupFavoritesCountResponse:", groupFavoritesCountResponse)

      res
        .set({
          'x-data-size': groupFavoritesCountResponse.length
        })
        .status(200)
        .json(groupFavoritesCountResponse);
    } catch (error) {
      log.info(error)

      next(error)
    }
  })

router.get('/categories/layouts', async (req, res, next) => {
  try {
    const groupRes = await layoutsDB.getGroupCountTh()

    res
      .status(200)
      .json(groupRes);
  } catch (error) {
    next(error)
  }
})


module.exports = router;
