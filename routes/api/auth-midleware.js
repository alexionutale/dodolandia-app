// this shit is not testable
// i need to move the auth to the nodejs app
const createError = require('http-errors');
const { logger: log } = require('../../utils/logger')


exports.verifyToken = async (req, res, next) => {
  try {
    const admin = req.app.locals.admin
    const bearer = req.headers['authorization']
    if (!bearer) throw createError(403, 'Authorization token is missing')


    const token = bearer.split('Bearer ')[1]
    console.time('verify auth token')
    const decodedToken = await admin.auth().verifyIdToken(token)
    console.timeEnd('verify auth token')

    req.user = await admin.auth().getUser(decodedToken.uid)

    req.auth = { ...decodedToken }
    next()
  } catch (error) {
    log.info(error)
    console.log(error)
    next(createError(403, error.message))
  }
}


exports.addUserAndAuthData = async (req, res, next) => {
  try {
    const admin = req.app.locals.admin
    const bearer = req.headers['authorization']
    if (!bearer) {
      console.log('token is not present, move on without error')
      next()
      return
    }

    const token = bearer.split('Bearer ')[1]
    console.time('addUserAndAuthData:verify auth token')
    const decodedToken = await admin.auth().verifyIdToken(token)
    console.timeEnd('addUserAndAuthData:verify auth token')
    req.user = await admin.auth().getUser(decodedToken.uid)

    req.auth = { ...decodedToken }
    next()
  } catch (error) {
    log.info(error)
    console.log(error)
    next(createError(error))
  }
}