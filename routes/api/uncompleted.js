const express = require('express');
const createError = require('http-errors');
const multer = require('multer')
const layoutsStorage = require('../../cloud-storage/layouts')

const router = express.Router();
const mUncompleted = require('../../mongodb/uncompleted.queries')
const { verifyToken } = require('./auth-midleware')

// create a new layout
const MULTER_UPLOAD_VALIDATION = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 20 * 1024 * 1024, // no larger than 20mb,
    files: 3,
    fields: 100
  },
})

router.get('/uncompleted/report', async (req, res, next) => {
  try {

    const groupRes = await mUncompleted.groupByTh()
    // consider a cache for this call
    res
      .status(200)
      .json(groupRes)

  } catch (error) {
    console.log(error)
    next(error)
  }
})

// this method is updating the entire layout, and converting it into a complete layout
router.put('/uncompleted/:id',
  // add user verification here
  verifyToken,
  MULTER_UPLOAD_VALIDATION.any(),
  async (req, res, next) => {
    try {
      if (req.params.id === undefined || req.params.id === null) throw createError(400, 'Please insert a valid params id.')
      if (!req.files.length) throw createError(422, 'No image to upload.')

      const layout = req.body
      const _id = req.params.id

      for (let i = 0; i < req.files.length; i++) {
        const image = req.files[i];
        await layoutsStorage.uploadImageFromData({ ...image, ...layout, uid: req.auth.uid, id: _id });
      }

      const updateResponse = await mUncompleted.updateUncompleted({ ...layout, id: _id }, req.auth.uid)
      res.set({ ...updateResponse, 'Access-Control-Expose-Headers': 'x-doc-id, x-layout-id' })
        .status(201).send('layout with image saved\n')
    } catch (error) {
      console.log(error)
      next(error)
    }
  })

// this method will flag the layouts that no longer exists in suppercell servers
router.put('/uncompleted/:id/flag',
  verifyToken,
  async (req, res, next) => {
    try {
      if (req.params.id === undefined || req.params.id === null) throw createError(400, 'Please insert a valid params id.')
      const _id = req.params.id

      const updateResponse = await mUncompleted.flagUncompletedLayout({ id: _id })
      res.set({ ...updateResponse, 'Access-Control-Expose-Headers': 'x-doc-id, x-layout-id' })
        .status(201).send('layout with image saved\n')
    } catch (error) {
      console.log(error)
      next(error)
    }
  })

module.exports = router;
