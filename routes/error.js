const multer = require('multer')
const { logger: log } = require('../utils/logger')

function errorHandler(err, req, res, next) {
  console.log('error handler triggered')
  log.error(err, err.message)
  if (err.code === 11000) {
    res.status(409).send('Duplicate key error');
    return;
  }
  if (err instanceof multer.MulterError) {
    res.status(400).send(err.message);
    return;
  }
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  log.info(err)
  log.info(err.message)
  // render the error page
  res.status(err.status || 500);
  res.send('something went wrong, check logs');
}

module.exports = errorHandler;
