const express = require('express');
const router = express.Router();
const layoutsDB = require('../mongodb/layouts.queries')

router.get('/dev/status', function (req, res, next) {
  const mem = process.memoryUsage()
  const os = require('os');

  res.status(200).json({
    mem,
    freemem: os.freemem(),
    totalmem: os.totalmem(),
    uptime: process.uptime(),
  });
});


module.exports = router;
