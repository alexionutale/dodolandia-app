const { client } = require('./connection')

const COLLECTION_NAME = 'layouts'
const DATABASE_NAME = `${(process.env.APP_ENV || 'development')}-bases`

/**
 * THIS SHOULD BE JUST VIEW ALL AND NOT CONTAIN ANY RESTRICTION 
 * maybe show only the layouts with public flag
 */

const defaultQuery = { '$or': [{ 'share': 'public' }], }
const defaultQueryUncompleted = { '$and': [{ 'publicLink': { '$exists': false } }] }

async function layoutsCollection() {
  const dbClient = await client()
  if (dbClient === null) {
    throw new Error('mongodb is not connected yet')
  }
  return dbClient.db(DATABASE_NAME).collection(COLLECTION_NAME)
}


/**
 * this scroll uses the id in desc order 
 * ( the id is generated from a timestamp so it should be generated in an incremental order)
 */
exports.scrollById_deprecated = async (skip = 0, size = 10, orderBy = 'downloads', asc = -1) => {
  const collection = await layoutsCollection()
  const res = await collection
    .find(defaultQuery)
    .sort({ [orderBy]: +asc })
    .skip(skip)
    .limit(size).toArray()
  return res
}

//
/**
 * this scroll uses the id in desc order (  this will also export the favorites )
 * ( the id is generated from a timestamp so it should be generated in an incremental order)
 */

exports.scrollByIdWithFavorites = async (skip = 0, size = 10, orderBy = 'downloads', asc = -1, uid, th = 'TH13', type = undefined) => {
  let query = { 'share': 'public' }
  // add this line for layouts that no longer exist into suppercell servers
  // flag: 'no-longer-exists'

  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: start')
  if (th !== undefined) query = { ...query, th: th }
  if (type !== undefined && type === 'BB') query = { ...query, type: type }

  let aggregateQuery = [
    {
      '$match': {
        ...query
      }
    }, {
      '$lookup': {
        'from': 'favorite-layouts',
        'localField': 'id',
        'foreignField': 'id',
        'as': 'favorite-layout'
      }
    }, {
      '$addFields': {
        'favorite': {
          '$filter': {
            'input': '$favorite-layout',
            'cond': {
              '$eq': [
                '$$this.uid', uid
              ]
            }
          }
        }
      }
    }, {
      '$unwind': {
        'path': '$favorite',
        'includeArrayIndex': 'string',
        'preserveNullAndEmptyArrays': true
      }
    }, {
      '$sort': {
        [orderBy]: +asc // try to apply the sort on this one
      }
    }, {
      '$skip': +skip
    }, {
      '$limit': +size
    },
    {
      '$project': {
        'description': 1,
        'link': 1,
        'publicLink': 1,
        'description': 1,
        'action': 1,
        'th': 1,
        'type': 1,
        'tag': 1,
        'share': 1,
        'userId': 1,
        'userPicture': 1,
        'tags': 1,
        'id': 1,
        'uid': 1,
        'createdOn': 1,
        'updateOn': 1,
        'image': 1,
        'favorite': 1,
        'downloads': 1
      }
    }
  ]
  //console.log('scrollByIdWithFavorites: aggregation query:', aggregateQuery)
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: query builded')

  const collection = await layoutsCollection()
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: collection is ready')

  let res = await collection
    .aggregate(aggregateQuery)
    .toArray()
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: fetched aggregation data')

  res = res.map(item => {
    const favItem = item.favorite
    return {
      ...item,
      isFavorite: (favItem && favItem.isFavorite) ? favItem.isFavorite : false
    }
  })
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: remaped response')

  console.log('scrollByIdWithFavorites:', res.length)
  return res
}

/**
 * this scroll uses the id in desc order 
 * ( the id is generated from a timestamp so it should be generated in an incremental order)
 */

exports.scrollByIdUncompletedLayout = async (skip = 0, size = 10, orderBy = 'downloads', asc = -1, uid, th = 'TH13', type = undefined) => {
  let query = { 'share': { '$exists': false }, flag: { '$ne': 'no-longer-exists' } }
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: start')
  if (th !== undefined) query = { ...query, th: th }
  if (type !== undefined && type === 'BB') query = { ...query, type: type }

  let aggregateQuery = [
    {
      '$match': {
        ...query
      }
    }, {
      '$lookup': {
        'from': 'favorite-layouts',
        'localField': 'id',
        'foreignField': 'id',
        'as': 'favorite-layout'
      }
    }, {
      '$addFields': {
        'favorite': {
          '$filter': {
            'input': '$favorite-layout',
            'cond': {
              '$eq': [
                '$$this.uid', uid
              ]
            }
          }
        }
      }
    }, {
      '$unwind': {
        'path': '$favorite',
        'includeArrayIndex': 'string',
        'preserveNullAndEmptyArrays': true
      }
    }, {
      '$sort': {
        [orderBy]: +asc // try to apply the sort on this one
      }
    }, {
      '$skip': +skip
    }, {
      '$limit': +size
    },
    {
      '$project': {
        'description': 1,
        'link': 1,
        'publicLink': 1,
        'description': 1,
        'action': 1,
        'th': 1,
        'type': 1,
        'tag': 1,
        'share': 1,
        'userId': 1,
        'userPicture': 1,
        'tags': 1,
        'id': 1,
        'uid': 1,
        'createdOn': 1,
        'image': 1,
        'favorite': 1,
        'downloads': 1
      }
    }
  ]
  // console.log('scrollByIdWithFavorites: aggregation query:', aggregateQuery)
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: query builded')

  const collection = await layoutsCollection()
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: collection is ready')

  let res = await collection
    .aggregate(aggregateQuery)
    .toArray()
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: fetched aggregation data')

  res = res.map(item => {
    const favItem = item.favorite
    return {
      ...item,
      isFavorite: (favItem && favItem.isFavorite) ? favItem.isFavorite : false
    }
  })
  console.log(Date(), (new Date()).getMilliseconds(), 'scrollByIdWithFavorites: remaped response')

  console.log('scrollByIdWithFavorites:', res.length)
  return res
}

/**
 * find one layout with id
 * 
 */

exports.getLayoutById = async (id) => {
  const collection = await layoutsCollection()
  const res = await collection.findOne({ id: id })
  console.log('getLayoutById:', res, id)
  return res
}


