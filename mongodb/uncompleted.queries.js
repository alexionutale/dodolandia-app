'use strict';
const { client } = require('./connection')
const createError = require('http-errors');

const COLLECTION_NAME = 'layouts'
const DATABASE_NAME = `${(process.env.APP_ENV || 'development')}-bases`

const insertOptions = { writeConcern: { w: "majority", wtimeout: 5000 } }

async function layoutsCollection() {
  const dbClient = await client()
  if (dbClient === null) {
    throw new Error('mongodb is not connected yet')
  }
  return dbClient.db(DATABASE_NAME).collection(COLLECTION_NAME)
}

exports.groupByTh = async () => {
  const coll = await layoutsCollection()

  const aggregate = [

    { "$match": { '$and': [{ 'publicLink': { '$exists': false } }], } },
    {
      "$group": {
        _id: { "th": "$th", "type": "$type" },
        count: { $sum: 1 }
      }
    }
  ]

  const res = await coll
    .aggregate(aggregate)
    .toArray()

  //remap th type
  const bases = res.map(base => {
    return {
      count: base.count,
      th: base._id.th,
      type: base._id.type === 'WB' ? 'HV' : base._id.type
    }
  })

  const remaped = bases.reduce((acc, item) => {
    const aKey = `${item.th}_${item.type}`
    const count = acc[aKey] || 0
    return { ...acc, [aKey]: count + item.count }
  }, {})

  const groupArr = Object.keys(remaped).map(key => {
    return {
      th: key.split('_')[0],
      type: key.split('_')[1],
      count: remaped[key]
    }
  })
  console.log(groupArr)
  return groupArr
}

// add new layout from uncompleted list

exports.updateUncompleted = async (layout, uid) => {
  const coll = await layoutsCollection()

  // find the layout based on original link
  const query = { link: layout.link }
  const oldLayout = await coll.findOne(query)

  let newLayout = {
    //...oldLayout,
    uid: uid,
    description: layout.description,
    longName: layout.longName,
    publicLink: layout.publicLink,
    share: layout.share,
    shortName: layout.shortName,
    tag: layout.tag,
    tags: layout.tags,
    updateOn: new Date(),
    createdOn: new Date(),
  }

  const updateRes = await coll.updateOne(
    query,
    {
      $set: {
        ...newLayout,
        updateOn: new Date()
      }
    },
    insertOptions
  )
  return { 'x-modified-count': updateRes.modifiedCount, 'x-matched-count': updateRes.matchedCount }
}


// flagUncompletedLayout
exports.flagUncompletedLayout = async (layoutId) => {
  const coll = await layoutsCollection()

  const query = { ...layoutId }

  let newLayout = {
    //...oldLayout,
    flag: 'no-longer-exists',
    updateOn: new Date(),
  }

  const updateRes = await coll.updateOne(
    query,
    {
      $set: {
        ...newLayout,
        updateOn: new Date()
      }
    },
    insertOptions
  )
  return { 'x-modified-count': updateRes.modifiedCount, 'x-matched-count': updateRes.matchedCount }
}
