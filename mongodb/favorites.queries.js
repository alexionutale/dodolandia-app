const { client } = require('./connection')

const COLLECTION_NAME = 'favorite-layouts'
const DATABASE_NAME = `${(process.env.APP_ENV || 'development')}-bases`

async function favoriteCollection() {
  const dbClient = await client()
  if (dbClient === null) {
    throw new Error('mongodb is not connected yet')
  }
  return dbClient.db(DATABASE_NAME).collection(COLLECTION_NAME)
}

/**
 * this is a toggle, that will add or remove from the favorites
 * adding: will also upsert the layout
 * removing: will only set the flag isFavorite to false
 */
exports.toggleLayoutAsFavorite = async (isFavorite, layoutId, uid) => {
  const collection = await favoriteCollection()

  const favoriteLayout = {
    id: layoutId,
    uid: uid
  }

  const res = await collection.updateOne(
    favoriteLayout,
    { "$set": { ...favoriteLayout, isFavorite } },
    {
      upsert: true,
    }
  )

  return res.result.ok
}

// consider adding a scrollPossibility
/**
 * get all the favorites for a specific user
 * it takes in only the user uid
 */
exports.getAllFavoritesForUID = async (skip = 0, size = 10, orderBy, asc = -1, uid, th = 'TH13', type = undefined) => {
  let aggregateQuery = [
    {
      '$match': {
        'uid': uid,
        'isFavorite': true
      }
    }, {
      '$lookup': {
        'from': 'layouts',
        'localField': 'id',
        'foreignField': 'id',
        'as': 'join_id'
      }
    }, {
      '$unwind': {
        'path': '$join_id',
        'includeArrayIndex': 'string',
        'preserveNullAndEmptyArrays': true
      }
    }, {
      '$match': {
        'join_id.th': th,
        'join_id.share': 'public'
      }
    }, {
      '$skip': +skip
    }, {
      '$limit': +size
    }
  ]
  console.log("getAllFavoritesForUID:", aggregateQuery)
  const collection = await favoriteCollection()
  let res = await collection
    .aggregate(aggregateQuery)
    .toArray()

  res = res.map(item => {
    const layItem = { ...(item.join_id) }
    delete item.join_id
    return {
      ...item,
      ...layItem
    }
  })
  console.log('returning layouts:', res.length)
  return res
}

exports.getGroupCountFavoritesTh = async (uid) => {
  let aggregate =
    [
      {
        '$match': {
          'uid': uid,
          'isFavorite': true
        }
      }, {
        '$lookup': {
          'from': 'layouts',
          'localField': 'id',
          'foreignField': 'id',
          'as': 'join_id'
        }
      }, {
        '$unwind': {
          'path': '$join_id',
          'includeArrayIndex': 'string',
          'preserveNullAndEmptyArrays': true
        }
      }, {
        '$group': {
          '_id': {
            "th": "$join_id.th", "type": "$join_id.type"
          },
          'count': {
            '$sum': 1
          }
        }
      }
    ]

  const coll = await favoriteCollection()
  const res = await coll
    .aggregate(aggregate)
    .toArray()

  //remap th type
  const bases = res.map(base => {
    return {
      count: base.count,
      th: base._id.th,
      type: base._id.type === 'WB' ? 'HV' : base._id.type
    }
  })

  const remaped = bases.reduce((acc, item) => {
    const aKey = `${item.th}_${item.type}`
    const count = acc[aKey] || 0
    return { ...acc, [aKey]: count + item.count }
  }, {})

  const groupArr = Object.keys(remaped).map(key => {
    return {
      th: key.split('_')[0],
      type: key.split('_')[1],
      count: remaped[key]
    }
  })

  console.log(groupArr)

  return groupArr
}
