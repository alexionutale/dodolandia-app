const MongoClient = require('mongodb').MongoClient

let mClient = null
exports.client = async () => {
  try {
    if (mClient && mClient.isConnected()) {
      return mClient
    }
    const uri = process.env.MONGODB_URI;
    mClient = await new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true })
    return mClient.connect()
  } catch (error) {
    throw error
  }
}
// at app start create the db connection
this.client()

// CAPTURE APP TERMINATION / RESTART EVENTS
// To be called when process is restarted or terminated
gracefulShutdown = function (msg, callback) {
  if (!mClient) return
  mClient.close(() => {
    console.log('Mongoose disconnected through ' + msg);
    callback();
  });
};


// For nodemon restarts
process.once('SIGUSR2', () => {
  gracefulShutdown('nodemon restart', () => {
    process.kill(process.pid, 'SIGUSR2')
  })
})

// For nodemon restarts
process.once('SIGUSRS2', () => {
  gracefulShutdown('nodemon restart', () => {
    process.kill(process.pid, 'SIGUSR2')
  })
})

// For app termination
process.on('SIGINT', () => {
  gracefulShutdown('app termination', () => {
    process.exit(0)
  })
})

// For Serverless app termination 
process.on('SIGTERM', () => {
  gracefulShutdown('Serverless app termination', () => {
    process.exit(0)
  })
})
