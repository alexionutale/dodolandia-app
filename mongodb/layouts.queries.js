const { client } = require('./connection')
const ObjectID = require('mongodb').ObjectID
const createError = require('http-errors');

const COLLECTION_NAME = 'layouts'
const DATABASE_NAME = `${(process.env.APP_ENV || 'development')}-bases`

const insertOptions = { writeConcern: { w: "majority", wtimeout: 5000 } }

async function layoutsCollection() {
  const dbClient = await client()
  if (dbClient === null) {
    throw new Error('mongodb is not connected yet')
  }
  return dbClient.db(DATABASE_NAME).collection(COLLECTION_NAME)
}

exports.generateObjectId = () => ObjectID().toString()

/**
 * insert new layout into the database
 */
exports.insert = async (layout, auth) => {
  const collection = await layoutsCollection()
  const atomObj = {
    ...layout,

    uid: auth.uid,
    userName: auth.name,
    userPicture: auth.picture,

    publicLink: layout.publicLink || layout.link,

    id: layout.id || ObjectID().toString(),
    updateOn: new Date(),
    createdOn: new Date(),
    importDate: new Date(),
  };

  const res = await collection.insertOne(
    atomObj,
    insertOptions
  )

  return { 'x-doc-id': res.insertedId, 'x-layout-id': res.ops[0].id }
}

exports.increaseDownloads = async (id) => {
  const collection = await layoutsCollection()
  const updateRes = await collection.updateOne(
    { id: id },
    { $inc: { downloads: 1 } },
    insertOptions
  )
  return { 'x-modified-count': updateRes.modifiedCount, 'x-matched-count': updateRes.matchedCount }
}

/**
 * update the metadata of the layout
 */
exports.update = async (body, user) => {
  const collection = await layoutsCollection()

  // find layout by link and uid
  const query = { link: body.link }
  const layout = await collection.findOne(query)

  if (!layout || layout.uid !== user.uid) throw createError(403, 'You can edit only the layouts that you own.')

  const l = {
    ...layout,
    description: body.description,
    longName: body.longName,
    publicLink: body.publicLink,
    share: body.share,
    shortName: body.shortName,
    tag: body.tag,
    tags: body.tags,
  }

  const updateRes = await collection.updateOne(
    query,
    {
      $set: {
        ...l,
        updateOn: new Date()
      }
    },
    insertOptions
  )
  return { 'x-modified-count': updateRes.modifiedCount, 'x-matched-count': updateRes.matchedCount }
}


/**
 * find layout in the database
 */
exports.findOne = async (layoutId) => {
  const collection = await layoutsCollection()
  return await collection.findOne({ id: layoutId })
}

/**
 * update just the main image
 */

exports.updateMainImage = async (layout) => {
  const coll = await layoutsCollection()
  const query = {
    link: layout.link,
    // TODO: 
    // uid: layout.uid disabled for now, but this needs better handling
  }

  const response = await coll.updateOne(
    query,
    {
      "$set": {
        [`image.${layout.imageSize}`]: layout.imageName
      }
    }
  )
  return response
}

/**
 * add YouTube videos of defences and attacks of base as example of how good is it
 */



// group count all TH types

exports.getGroupCountTh = async () => {
  const coll = await layoutsCollection()

  const aggregate = [
    { "$match": { '$or': [{ "share": "public" }], } },
    {
      "$group": {
        _id: { "th": "$th", "type": "$type" },
        count: { $sum: 1 }
      }
    }
  ]


  const res = await coll
    .aggregate(aggregate)
    .toArray()


  //remap th type
  const bases = res.map(base => {
    return {
      count: base.count,
      th: base._id.th,
      type: base._id.type === 'WB' ? 'HV' : base._id.type

    }
  })


  const remaped = bases.reduce((acc, item) => {
    const aKey = `${item.th}_${item.type}`
    const count = acc[aKey] || 0
    return { ...acc, [aKey]: count + item.count }
  }, {})


  const groupArr = Object.keys(remaped).map(key => {
    return {
      th: key.split('_')[0],
      type: key.split('_')[1],
      count: remaped[key]
    }
  })
  console.log(groupArr)
  return groupArr
}
