const sharp = require('sharp')

/**
 * Resize Image
 * (imageBuff)
 * return buffer 
 */
exports.resize = async (imageBuff, size) => {
  return await sharp(imageBuff).resize(size).toBuffer()
}

/**
 * watermark image (image: Buffer)
 * return
 */
exports.watermarkImage = async (imageBuff) => {
  // TODO: make the size of the watermark image a percentage of the main layout
  // this should make text smaller on thumbnails
  const options = {
    raw: {
      width: 300,
      height: 300,
      channels: 4
    }
  }
  const logo = await sharp(__dirname + '/logo-white.png', options)
    .resize(300)
    .toBuffer()

  const layout = sharp(imageBuff)
    .composite([{ input: logo, gravity: 'west' }, { input: logo, gravity: 'east' }, { input: logo, gravity: 'center' }])
    .withMetadata()

  return await layout.toBuffer()
}
