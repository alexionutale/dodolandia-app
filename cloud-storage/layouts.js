const { Storage } = require('@google-cloud/storage')
const imagesLib = require('./lib/edit-image')
const layoutDb = require('../mongodb/layouts.queries')

// declaring constants ( consider using a config file )
const BUCKET_NAME = process.env.BUCKET_NAME
const BUCKET_NAME_ORIGINALS = process.env.BUCKET_NAME_ORIGINALS

/**
 * upload multiple images from the req.files
 * images<Object>
 */
exports.uploadImages = async (images) => {
  Object.keys(images).forEach(imageName => {
    console.log(imageName)
  })
}

/**
 * data is the file as bufer data
 */
exports.uploadImageFromData = async (layoutImage) => {
  try {
    console.time('process image' + layoutImage.id)

    const storage = await uploadOriginalImage(layoutImage)

    const wImage = await watermarkImage(layoutImage)

    await uploadWatermarkImage(storage, layoutImage, wImage)

    // register the image path into the db document
    await layoutDb.updateMainImage({ ...layoutImage, imageName: watermarkPath(layoutImage.id), imageSize: 'original' })

    createThumbnailsAsync(storage, layoutImage, wImage)

    console.log('finish processing image')
    return true
  } catch (error) {
    console.log("catch error:", error)
  } finally {
    console.timeEnd('process image' + layoutImage.id)
  }

  return false
}

async function uploadWatermarkImage(storage, layoutImage, watermarkImage) {
  console.time('upload watermark' + layoutImage.id)
  await storage.bucket(BUCKET_NAME).file(watermarkPath(layoutImage.id)).save(watermarkImage)
  console.timeEnd('upload watermark' + layoutImage.id)
}

async function watermarkImage(layoutImage) {
  console.time('watermark original' + layoutImage.id)
  const watermarkImage = await imagesLib.watermarkImage(layoutImage.buffer)
  console.timeEnd('watermark original' + layoutImage.id)
  return watermarkImage
}

async function uploadOriginalImage(layoutImage) {
  console.time('upload original ' + layoutImage.id)
  const storage = new Storage()
  await storage.bucket(BUCKET_NAME_ORIGINALS).file(originalPath(layoutImage.id)).save(layoutImage.buffer)
  console.timeEnd('upload original ' + layoutImage.id)
  return storage
}

/**
 * resize and upload thumbnails asynchronously 
 * maybe this part could be moved to a 
 */
function createThumbnailsAsync(storage, layoutImage, wImage) {
  const sizes = [64, 128, 256, 512, 768, 1024, 1280, 1600, 1920, 2432]
  console.info(sizes.length)

  sizes.map(async size => {
    console.time(thumbSizePath(size, layoutImage.id))
    try {
      const newSizeImageBuffer = await imagesLib.resize(wImage, size)
      const nameWithSize = thumbSizePath(size, layoutImage.id)
      storage.bucket(BUCKET_NAME).file(nameWithSize).save(newSizeImageBuffer)

      await layoutDb.updateMainImage({ ...layoutImage, imageName: nameWithSize, imageSize: `thumb_${size}` })

    } catch (error) {
      console.error(error, `unable to resize thumbnail ${`thumb@${size}_${layoutImage.id}`}`)
    }
    console.timeEnd(thumbSizePath(size, layoutImage.id))
  })
}

const watermarkPath = (id) => `watermark/${id}`
const originalPath = (id) => `original/${id}`
const thumbSizePath = (size, id) => `thumb@${size}/${id}`


